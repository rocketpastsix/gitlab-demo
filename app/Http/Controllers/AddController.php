<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;

class AddController extends Controller
{
    public function index()
    {
        return view('new-quote');
    }

    public function create(Request $request)
    {
        $quote = $request->get('quote');
        $author = $request->get('author');

        $model = new Quote();

        $model->quote = $quote;
        $model->author = $author;

        $model->save();

        return view('added-quote');
    }
}