<?php

namespace App\Http\Controllers;

use App\Quote;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use App\Api\QuoteTransformer;
use Illuminate\Contracts\Routing\ResponseFactory;

class QuotesController extends Controller
{
    /**
     * @var QuoteTransformer
     */
    private $transformer;

    /**
     * @var ResponseFactory
     */
    private $response;

    /**
     * QuotesController constructor.
     * @param QuoteTransformer $transformer
     * @param ResponseFactory $response
     */
    public function __construct(QuoteTransformer $transformer, ResponseFactory $response)
    {
        $this->transformer = $transformer;
        $this->response = $response;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $quote = Quote::inRandomOrder()->limit(1)->get();

        return view('quote', ['quote' => $quote]);
    }

    /**
     * @param Request $request
     */
    public function sendQuote(Request $request)
    {
        $phoneNumber = $request->get('phone');

        $quote = Quote::inRandomOrder()->limit(1)->pluck('author', 'quote');

        $client = new Client(
            env('TWILIO_ACCT_SID'),
            env('TWILIO_ACCT_TOKEN')
        );

        $client->messages->create(
            $phoneNumber, [
                'from' => env('TWILIO_NUMBER'),
                'body' => $quote
            ]
        );

        return view('after-quote');
    }
}
