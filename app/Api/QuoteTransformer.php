<?php

namespace App\Api;

use App\Quote;
use League\Fractal\TransformerAbstract;

class QuoteTransformer extends TransformerAbstract
{
    public function transform($quote)
    {
        return [
            'quote' => $quote->quote,
            'author' => $quote->author
        ];
    }
}