@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-offset-2">
                <form method="post" action="{{url('/')}}/add">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-1">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="quote">Quote</label>
                                <textarea cols="5" rows="5" class="form-control" name="quote" id="quote"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="author">Author</label>
                                <input type="text" class="form-control" name="author" id="author">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" id="button">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


@endsection