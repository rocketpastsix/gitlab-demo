@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-offset-3">
                <h3>Enter your phone number and get a famous quote!</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="{{url('/')}}/quote" id="quoteForm">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="tel" class="form-control" name="phone" id="phone" placeholder="111-111-1111">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" id="button">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <hr>
                @foreach($quote as $saying)
                    <p class="text-center"> {{ $saying->quote }} </p>
                    <p class="text-center"> <em>{{ $saying->author }}</em></p>
                @endforeach
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('button#button').on('click', function() {

            let data = $('input#phone').serialize();

            $.ajax({
                data: data,
                cache: false,
                url: '{{url('/')}}/quote',
                success: function() {
                    console.log('success');
                },
                failure: function() {
                    console.log(error);
                },
            })
        });

    </script>

@endsection