@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-offset-2">
                <p>Quote was sent</p>

                <button class="btn" onclick="goBack()">Get Another</button>
            </div>
        </div>


        <script>
            function goBack() {
                window.history.back();
            }
        </script>

@endsection