<?php

use Illuminate\Database\Seeder;

class QuotesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert([
            'author' => 'Wayne Gretzky',
            'quote' => 'You miss 100% of the shots you don’t take.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Amelia Earhart',
            'quote' => 'The most difficult thing is the decision to act, the rest is merely tenacity.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Christopher Columbus',
            'quote' => 'You can never cross the ocean until you have the courage to lose sight of the shore.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Alexander Hamilton',
            'quote' => 'I never expect to see a perfect work from imperfect man.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'James Madison',
            'quote' => 'All men having power ought to be distrusted to a certain degree.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'John Paul Jones',
            'quote' => 'I have not yet begun to fight!'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Patrick Henry',
            'quote' => 'Give me liberty or give me death.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Henry David Thoreau',
            'quote' => 'Go confidently in the direction of your dreams.  Live the life you have imagined.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Sheryl Sandberg',
            'quote' => 'If you\'re offered a seat on a rocket ship, don\'t ask what seat! Just get on.'
        ]);
        DB::table('quotes')->insert([
            'author' => 'Steve Jobs',
            'quote' => 'The only way to do great work is to love what you do.'
        ]);
    }
}
